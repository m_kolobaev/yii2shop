<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>


	<?= $form->field($model, 'parent_id')->dropdownList(Categories::getAllCategoryArr(),
        ['prompt'=>'Select Category']
    ); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>   

    <?= $form->field($model, 'info')->textarea(['rows' => 15, 'cols' => 5]) ?>

    <?= $form->field($model, 'img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
