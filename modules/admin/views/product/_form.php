<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    
    <?= $form->field($model, 'category_id')->dropdownList(Categories::getAllCategoryArr(),
        ['prompt'=>'Select Category']
    ); ?>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>
    
    <?= $form->field($model, 'img')->textInput() ?>
    
    <?= $form->field($model, 'imageFile')->fileInput() ?>
    

    <?= $form->field($model, 'info')->textarea(['rows' => 15, 'cols' => 5]) ?>
    
    
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>


    
    
    <?// $form->field($model, 'id_category')->textInput() ?>

    <?= $form->field($model, 'brand_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
