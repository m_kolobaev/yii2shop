<?php

namespace app\modules\admin\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use yii\bootstrap\Nav;

class NavBarModuleWidget extends Widget
{
    public $message;
    
    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = 'Hello World';
        }
    }
    
    public function run()
    {
        
        $navItems = Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'Products', 'url' => ['/admin/product']],
                ['label' => 'Category', 'url' => ['/admin/category']],
                \Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                    ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . \Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                            )
                        . Html::endForm()
                        . '</li>'
                        )
            ],
            'encodeLabels' => false,
        ]);
        
        
         
        $htmlFirst = 
        
        Html::tag('header',
            Html::tag('div', 
                Html::tag('div',
                    '111', 
                ['class' => 'container']),
            ['class' => 'header']),
        ['id' => 'page-topbar']);
        
        $htmlSecond =
        
               Html::tag('div', 
                   Html::tag('nav',
                       $navItems ,
                   [ 'class' => 'navbar navbar-light navbar-expand-lg topnav-menu' ]) , 
            [ 'class' => 'container']);
        
        
        $html = $htmlFirst . $htmlSecond;
        
        return $html . $nav;
    }
}