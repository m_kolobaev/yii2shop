<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii2_product_options_var".
 *
 * @property int|null $product_id
 * @property int|null $option_var_id
 */
class ProductOptionsVar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_product_options_var';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'option_var_id'], 'integer'],
            [['product_id', 'option_var_id'], 'unique', 'targetAttribute' => ['product_id', 'option_var_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'option_var_id' => 'Option Var ID',
        ];
    }
}
