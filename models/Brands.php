<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii2_brands".
 *
 * @property int $id
 * @property string $name Наименование бренда
 * @property string|null $description Мета-тег description
 * @property string|null $img Имя файла изображения
 * @property string|null $keywords Мета-тег keywords
 */
class Brands extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%_brands}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description', 'img', 'keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'img' => 'Img',
            'keywords' => 'Keywords',
        ];
    }
    
    
    /**
     * Метод возвращает массив товаров бренда
     */
    public function getProducts() {
        // связь таблицы БД `brand` с таблицей `product`
        return $this->hasMany(Products::class, ['brand_id' => 'id']);
    }
    
    /**
     * Возвращает информацию о бренде с идентификатором $id
     */
    public function getBrand($id) {
        $id = (int)$id;
        return self::findOne($id);
    }
    

    /**
     * Возвращает массив всех брендов каталога и
     * количество товаров для каждого бренда
     */
    public function getAllBrands() {
        $query = self::find();
        $brands = $query
        ->select([
            'id' => 'brand.id',
            'name' => 'brand.name',
            'content' => 'brand.content',
            'img' => 'brand.image',
            'count' => 'COUNT(*)'
        ])
        ->innerJoin(
            'product',
            'product.brand_id = brand.id'
            )
            ->groupBy([
                'brand.id', 'brand.name', 'brand.content', 'brand.image'
            ])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();
            return $brands;
    }
    

    
}
