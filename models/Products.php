<?php
namespace app\models;

use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $price
 * @property int $id_category
 * @property string|null $description
 */
class Products extends \yii\db\ActiveRecord
{

    /**
     *
     * @var UploadedFile
     */
    public $imageFile;
    public $url = '/sa';
    /**
     *
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     *
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'code',
                    'price'
                ],
                'required'
            ],
            [
                [
                    'price'
                ],
                'integer'
            ],
            [
                [
                    'description'
                ],
                'string'
            ],
            [
                [
                    'url',
                    'name',
                    'slug',
                    'code'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'info'
                ],
                'string'
            ],
            [
                [
                    'category_id',
                    'category_id'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'brand_id',
                    'brand_id'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'img',
                    'img'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'imageFile'
                ],
                'file',
                'extensions' => 'png, jpg'
            ]
        ];
    }

    /**
     *
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'price' => 'Price',
            'description' => 'Description',
            'category_id' => 'category',
            'brand_id' => 'brand',
            'slug' => 'slug',
            'img' => 'img'
        ];
    }

    /**
     * Возвращает родительскую категорию
     */
    public function getCategory()
    {
        // связь таблицы БД `product` с таблицей `category`
        return $this->hasOne(Categories::class, [
            'id' => 'category_id'
        ]);
    }

    /**
     * Возвращает reviews
     */
    public function getReviews()
    {
        // связь таблицы БД `product` с таблицей `category`
        return $this->hasMany(ProductReviews::class, [
            'product_id' => 'id'
        ]);
    }

    /**
     * Вспомогательная связь тблицы многие ко многим OptionVar
     */
    public function getOptionsVar()
    {
        return $this->hasMany(OptionsVar::className(), [
            'id' => 'option_var_id'
        ])
            ->viaTable('{{%product_options_var}}', [
            'product_id' => 'id'
        ])
            ->with('option');
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert && ! empty($this->imageFile) && $this->validate()) {
                $nameTime = time();
                $this->imageFile->saveAs(Yii::getAlias('@webroot') . '/files/img/product/' . $nameTime . '.' . $this->imageFile->extension);
                $this->img = $nameTime . '.' . $this->imageFile->extension;
                return true;
            }
            return true;
        } else {
            return false;
        }
    }

    static function getStarsIcon($s = 0)
    {
        for ($i = 0; $i < 5; $i ++) {
            if ($i >= $s) {
                $stars .= '<span class="icon-ratings"><i class="fa fa-star-o"></i></span>';
            } else {
                $stars .= '<span class="icon-ratings"><i class="fa fa-star"></i></span>';
            }
        }

        return $stars;

        /*
         * <span
         * class="icon-ratings"><i class="fa fa-star"></i></span> <span
         * class="icon-ratings"><i class="fa fa-star-o"></i></span>
         */
    }

    /*
     * public function afterSave($insert, $changedAttributes)
     * {
     * parent::afterSave($insert, $changedAttributes);
     * if (! empty($this->imageFile)) {
     * if ($this->validate()) {
     * $this->imageFile->saveAs(Yii::getAlias('@webroot') . '/files/img/product/' . time() . '.' . $this->imageFile->extension);
     * return true;
     * } else {
     * return false;
     * }
     *
     * $this->img = $this->imageFile->name;
     * $this->save();
     * }
     * }
     */
}
