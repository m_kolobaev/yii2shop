<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii2_options_var".
 *
 * @property int $id
 * @property int|null $option_id
 * @property string $name
 */
class OptionsVar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_options_var';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['option_id'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'option_id' => 'Option ID',
            'name' => 'Name',
        ];
    }
    
    /**
     * Возвращает опцию опции
     */
    public function getOption() {
        // связь таблицы БД `product` с таблицей `category`
        return $this->hasOne(Options::class, ['id' => 'option_id']);
    }
}
