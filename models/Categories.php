<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "yii2_categories".
 *
 * @property int $id
 * @property int|null $parent_id Родительская категория
 * @property string $name Наименование категории
 * @property string|null $description Мета-тег description
 * @property string|null $img Имя файла изображения
 * @property string|null $keywords Мета-тег keywords
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['name'], 'required'],
            [['name', 'description', 'img', 'keywords'], 'string', 'max' => 255],
            [['info'], 'string'],
            [['slug'], 'slug'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Родитель',
            'name' => 'Name',
            'description' => 'Description',
            'img' => 'Img',
            'keywords' => 'Keywords',
            'info' => 'info',
            'slug' => 'slug',
        ];
    }
    
    
    /**
     * Возвращает названия и id всеx категорий массивом
     */
    public static function getAllCategoryArr() {
        
        $arr = [];
        $categories = self::find()->select(['id', 'name', 'parent_id'])->indexBy('id')->asArray()->all();
        
        foreach ($categories as $ctegoryId => &$categoryItem ){
            if (  $categoryItem['parent_id']) {
                $categoryItem['name'] = $categories[$categoryItem['parent_id']]['name'] . ' > ' . $categoryItem['name'];               
            }
            $arr[$ctegoryId] = $categoryItem['name'];
        }
        
        return $arr;
//        return ArrayHelper::map(self::find()->indexBy('id')->all(), 'id', 'name') ;
    }
    
    /**
     * Возвращает товары категории
     */
    
    public $ofsetProducts = -1;
    public $limitProducts = -1;
    
    public function getProducts() {
        // связь таблицы БД `category` с таблицей `product`
       // echo $this->ofsetProducts;
        return $this->hasMany(Products::class, ['category_id' => 'id'])->offset($this->ofsetProducts)->limit($this->limitProducts);
    }
    
    /**
     * Возвращает количество товаров
     */
    public function getCountProducts() {
        // связь таблицы БД `category` с таблицей `product`
        return $this->hasMany(Products::class, ['category_id' => 'id'])->count();
    }
    

    
    /**
     * Возвращает родительскую категорию
     */
    public function getParent() {
        // связь таблицы БД `category` с таблицей `category`
        return $this->hasOne(self::class, ['id' => 'parent_id']);
    }
    
    
    /**
     * Возвращает имя родительской категории
     */
    public function getParentName() {
        // связь таблицы БД `category` с таблицей `category`
        if( ! empty($this->getParent()->one()->name) && isset($this->getParent()->one()->name)){
            return $this->getParent()->one()->name; 
        }
        return 'Каталог (root)';   
        //return $this->hasOne(self::class, ['id' => 'parent_id']);
    }
    
    /**
     * Возвращает дочерние категории
     */
    public function getChildren() {
        // связь таблицы БД `category` с таблицей `category`
        return $this->hasMany(self::class, ['parent_id' => 'id']);
    }
}
