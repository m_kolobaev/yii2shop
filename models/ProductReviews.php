<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii2_product_reviews".
 *
 * @property int $id
 * @property int $product_id
 * @property int $useer_id
 * @property int|null $stars
 * @property int $data
 * @property string $reviews
 */
class ProductReviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_product_reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'useer_id', 'data', 'reviews'], 'required'],
            [['product_id', 'useer_id', 'stars', 'data'], 'integer'],
            [['text'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'useer_id' => 'Useer ID',
            'stars' => 'Stars',
            'data' => 'Data',
            'text' => 'Text',
        ];
    }

    function getStarsIcon()
    {
        for ($i = 0; $i < 5; $i ++) {
            if($this->stars <= $i){
                $stars .= '<span class="icon-ratings"><i class="fa fa-star-o"></i></span>';
            }else{
                $stars .= '<span class="icon-ratings"><i class="fa fa-star"></i></span>';
            }
        }
        
        return $stars;
        
      /*  <span
        class="icon-ratings"><i class="fa fa-star"></i></span> <span
        class="icon-ratings"><i class="fa fa-star-o"></i></span>*/
    }
}
