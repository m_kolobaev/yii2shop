<?php
namespace app\components;

use yii\web\UrlRuleInterface;
use yii\base\BaseObject;
use app\models\Categories;
use app\models\Products;

class UrlRule extends BaseObject implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {        
        
        
        if ($route === 'catalog/view') {
            $cid = $params['id'];
            $cache = \Yii::$app->cache;        
            $key = 'CategoryID-' . $cid;
            $category = $cache->getOrSet($key, function () use ($cid) {
                return Categories::find()->where('id=:id', [':id' => $cid])->with(['parent'])->one();
            }, 3600);
                        
            $categorySlug = $this->parentCategory($category, $category->slug) ;
            return  '/catalog/' . $categorySlug;
        }
        
        if ($route === 'product/view') {
            $pid = $params['id'];
            $cache = \Yii::$app->cache;
            $key = 'ProductID-' . $pid;
            $product = $cache->getOrSet($key, function () use ($pid) {
                return Products::find()->where('id=:id', [':id' => $pid])->with(['category'])->one();
            }, 3600);
            
            $productSlug = $product->slug;
            
            $categorySlug = $this->parentCategory($product->category, $product->category->slug) ;
            
            return  '/catalog/' .$categorySlug . '/'. $productSlug;
        }
        
        
        return false;
    }

    public function parseRequest($manager, $request)
    {        
        
        
        $pathInfo = $request->getPathInfo();
        $URLs = explode("/", $pathInfo);
        $URL_Last = $URLs[count($URLs)-1];
      
        if( $URLs[0] == 'catalog'){
            
            $cache = \Yii::$app->cache;
            $key = 'categorySlug-' . $URLs[count($URLs)-1];
            $category = $cache->getOrSet($key, function () use ($URL_Last) {
                return Categories::find()->where('slug=:slug', [':slug' => $URL_Last])->one();
            }, 3600);
            
            
            $categoryId = $category->id;
            
            if(!empty($categoryId)){
                return ['catalog/view', ['id'=> $categoryId]];
            }
            
            
            $cache = \Yii::$app->cache;
            $keyProd = 'productSlug-' . $URLs[count($URLs)-1];
            $product = $cache->getOrSet($keyProd, function () use ($URL_Last) {
                return Products::find()->where('slug=:slug', [':slug' => $URL_Last])->one();
            }, 3600);
            
                $productId = $product->id;
            
            if(!empty($productId)){
                return ['product/view', ['id'=> $productId]];
            }
        }
        
        
        return false;
    }
    
    private function parentCategory($category, $slug = ''){ 
      
        if(isset($category->parent_id)){           
            $cid = $category->parent_id;
            $cache = \Yii::$app->cache;
            $key = 'CategoryID-' . $cid;
            $categoryParent = $cache->getOrSet($key, function () use ($category) {
                return $category->parent;
            }, 3600);
            
            
            //$categoryParent = $category->parent;
                $slug = $categoryParent->slug . '/' . $slug;
                $slug = $this->parentCategory( $categoryParent,  $slug);
        }
        
        
        
        return $slug;
        
    }
}