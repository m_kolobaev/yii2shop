<?php
namespace app\components;

use yii\web\UrlRuleInterface;
use yii\base\BaseObject;
use app\models\Categories;
use app\models\Products;

class UrlRule extends BaseObject implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {        
        $id = $params['id'];
        $cache = \Yii::$app->cache;
        
        
        if ($route === 'catalog/view') {
            
            $key = 'CategoryId-' . $id;
            $category = $cache->getOrSet($key, function () use ($id) {
                return Categories::find()->where('id=:id', [':id' => $id])->select('id, slug, parent_id, name')->one();
            }, 3600);
            
           
            $categorySlug = $this->parentCategory($category, $category->slug) ;
            return  '/catalog/' . $categorySlug;
        }
        
        if ($route === 'product/view') {
            $product = Products::find()->where('id=:id', [':id' => $id])->select('id, category_id, slug')->one();        
            $productSlug = $product->slug;            
            
            $key = 'CategoryId-' . $product->category_id;
            $categoryP = $cache->getOrSet($key, function () use ($product) {
                return $product->category;
            }, 3600);
            
                $categorySlug = $this->parentCategory($categoryP, $categoryP->slug) ; 
            
           
            return  '/catalog/' .$categorySlug . '/'. $productSlug;
        }
        
        
        return false;
    }

    public function parseRequest($manager, $request)
    {           
        $pathInfo = $request->getPathInfo();
        $URLs = explode("/", $pathInfo);
        $URL_Last = $URLs[count($URLs)-1];
        $cache = \Yii::$app->cache;
        
        if( $URLs[0] == 'catalog'){
            
            $key = 'categorySlug-' . $URLs[count($URLs)-1];
            $category = $cache->getOrSet($key, function () use ($URL_Last) {
                return Categories::find()->where('slug=:slug', [':slug' => $URL_Last])->one();
            }, 3600);
            
            $category = Categories::find()->where('slug=:slug', [':slug' => $URL_Last])->one();
            $categoryId = $category->id;
            
            if(!empty($categoryId)){
                return ['catalog/view', ['id'=> $categoryId]];
            }
            
            
            $product = Products::find()->where('slug=:slug', [':slug' => $URL_Last])->one();
            
                $productId = $product->id;
            
            if(!empty($productId)){
                return ['product/view', ['id'=> $productId]];
            }
        }
        
        
        return false;
    }
    
    private function parentCategory($category, $slug = ''){ 
        $cache = \Yii::$app->cache;
        if(isset($category->parent_id)){           
            $parentId = $category->parent_id; 
            
            $key = 'CategoryId-' . $parentId;
            $categoryParent = $cache->getOrSet($key, function () use ($category) {
                return $category->parent;
            }, 3600);
            
            $slug = $categoryParent->slug . '/' . $slug;
            $slug = $this->parentCategory( $categoryParent,  $slug);
        }
        
        return $slug;
        
    }
}