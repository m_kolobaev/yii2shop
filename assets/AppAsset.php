<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
   // public $basePath = '@webroot';
   // public $baseUrl = '@web';
    public $sourcePath = '@app/assets'; 
    
    public $css = [
        'css/site.css',
        'css/font-awesome.min.css',
        'css/default.css',
        'css/header.css',
        
    ];
    public $js = [
        'js/jqueryCookie.js',
        'js/site.js',
        
        //react
          '//cdnjs.cloudflare.com/ajax/libs/react/0.14.0/react.js',
           '//cdnjs.cloudflare.com/ajax/libs/react/0.14.0/react-dom.js',
        
        /* production
            '//unpkg.com/react@16/umd/react.production.min.js',
            '//unpkg.com/react-dom@16/umd/react-dom.production.min.js',
        */
           '//cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser.js',
        
            

        
        
        //jsx
        ['jsx/view-catalog.js', 'type'=>'text/babel' ],
        
        

    ];
    public $depends = [
        '\rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset', // подключение Bootstrap        
        'yii\bootstrap4\BootstrapPluginAsset'
    ];
}
