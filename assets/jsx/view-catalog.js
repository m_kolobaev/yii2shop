/* ToolBar */
class ToolBar extends React.Component {

    render() {
        var countProducts = this.props.countProducts;
        return <div className="tool-bar row">
            <div className="col-md-6 col-xs-6">
                <LeftButtons onClick={this.props.onClickShowBlock}/>
            </div>
            <div className="col-md-3 col-xs-3 text-right">
                <SortByBlock/>
            </div>
            <div className="col-md-3 col-xs-3 text-right">
                <ShowBlock countProducts={countProducts} onChange={this.props.onChangeShow}/>
            </div>
        </div>;
    }
}

class LeftButtons extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stateShow: 'grid',
        };

        this.showBlock = this.showBlock.bind(this);

    }

    showBlock(event) {
        this.setState({stateShow: event.currentTarget.dataset.showblock});
        this.props.onClick(event.currentTarget);
    }


    button(props) {
        if (props.active) {
            return <button data-showblock={props.dataShowBlock} onClick={props.onClick} type="button" onclick=""
                           className="btn btn-default btn-custom-view btn-grid-1 active"><i
                className={props.faClass}></i></button>
        } else {
            return <button data-showblock={props.dataShowBlock} onClick={props.onClick} type="button" onclick=""
                           className="btn btn-default btn-custom-view btn-grid-1"><i className={props.faClass}></i>
            </button>
        }

    }

    render() {
        var stateShow = this.state.stateShow;

        if (stateShow == 'grid') {
            return (
                <div className="btn-group btn-group-sm">
                    <this.button active={true} dataShowBlock="grid" faClass="fa fa-th" onClick={this.showBlock}/>
                    <this.button active={false} dataShowBlock="list" faClass="fa fa-th-list" onClick={this.showBlock}/>
                </div>
            );
        } else {
            return (
                <div className="btn-group btn-group-sm">
                    <this.button active={false} dataShowBlock="grid" faClass="fa fa-th" onClick={this.showBlock}/>
                    <this.button active={true} dataShowBlock="list" faClass="fa fa-th-list" onClick={this.showBlock}/>
                </div>
            );
        }
    }
}


//ShowBlock
class ShowBlock extends React.Component {
    render() {
        var countProducts = this.props.countProducts;
        var ShowLimitProducts = [4, 6, 12, 26, 52, 74];
        var optionsArr = ShowLimitProducts.map(function (num) {
            return <option key={num} value={num}>{num}</option>;
        });
        return <div className="form-group input-group input-group-sm ">
            <label className="input-group-addon" htmlFor="input-limit-products">Show:</label>
            <select defaultValue={countProducts} id="input-limit-products" className="form-control"
                    onChange={this.props.onChange}>
                {optionsArr}
            </select>
        </div>;
    }
}

//SortByBlock
class SortByBlock extends React.Component {
    render() {
        var SortBy = ["name", "price", "stars"];
        var optionsArr = SortBy.map(function (SortByParam) {
            return <option key={SortByParam} value={SortByParam}>{SortByParam}</option>;
        });

        return <div className="form-group input-group input-group-sm ">
            <label className="input-group-addon" htmlFor="input-sort">Sort By:</label>
            <select id="input-sort" className="form-control">
                {optionsArr}
            </select>
        </div>;
    }
}

/* Products */
class Products extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: this.props.currentPage,
        }
        
        this.setPageNum = this.setPageNum.bind(this);
        this.setPagePrev = this.setPagePrev.bind(this);
        this.setPageNext = this.setPageNext.bind(this);
    }
    
    setPageNum(event) {
        this.setState({
            currentPage: event.target.textContent
        });
    }

    setPagePrev(event) {
        let currentPagePrev = this.state.currentPage > 1 ? this.state.currentPage - 1 : this.state.currentPage;
        this.setState({
            currentPage: currentPagePrev
        });
    }

    setPageNext(event) {
        let _currentPage = parseInt(this.state.currentPage);
        let currentPageNext = Math.ceil(this.state.products.length / this.props.countProducts) > _currentPage ? _currentPage + 1 : this.state.currentPage;

        this.setState({
            currentPage: currentPageNext
        });

    }


    getClassProductBlock() {
        let _type = this.props.typeShowBlock;
        let _classShowBlock = 'col-sm-3';
        if (_type == 'list') {
            _classShowBlock = 'col-sm-12';
        } else if (_type == 'grid') {
            _classShowBlock = 'col-sm-3';
        }

        return _classShowBlock;

    }

    render() {

        var products = this.props.products;
        var countProducts = this.props.countProducts;
        var currentPage = this.state.currentPage;
        if (Math.ceil(products.length / this.props.countProducts) < currentPage) {
            currentPage = 1;
        }

        const indexOfLastTodo = currentPage * countProducts;
        const indexOfFirstTodo = indexOfLastTodo - countProducts;

        var currentTodos = products.slice(indexOfFirstTodo, indexOfLastTodo);


        var productsItems = currentTodos.map((product, index) => (
            <div key={index} className={this.getClassProductBlock()}>
                <Product product={product} typeShow={this.props.typeShowBlock}/>
            </div>
        ))
        
        return (<div>
                <Pagenation
                    onClickNumPage={this.setPageNum}
                    onClickPrev={this.setPagePrev}
                    onClickNext={this.setPageNext}
                    currentPage={currentPage}
                    countAllProducts={products.length}
                    countProducts={this.props.countProducts}
                />
                <div className="row product-thumb">
                    {productsItems}
                </div>
            </div>
        );
    }
}

class Product extends React.Component {

    render() {

        if (this.props.typeShow == "grid") {
            return <ProductGrid product={this.props.product}/>
        } else if (this.props.typeShow == "list") {
            return <ProductList product={this.props.product}/>
        }


    }
}

class ProductGrid extends React.Component {

    getImgScr(){
        return '/files/img/product/' +  this.props.product.img;
    }

    render() {
    	var formatter = new Intl.NumberFormat('ru-RU', {
			  style: 'currency',
			  currency: 'RUB',
			});
    	var price = formatter.format(this.props.product.price);
        return (
            <div className="grid-style">
                <div className="product-item">
	                <div className="image ">
	                    <a href={this.props.product.url}>
	                        <img className="img-responsive" src={this.getImgScr()} alt="Product" />
	                    </a>                        
	                    <div className="button-group">	                    		
								<div className="inner">
								<ButtonProduct btnClass="button-cart" faClass="fa fa-cart-plus"  />
								<ButtonProduct btnClass="button-quickview" faClass="fa fa-search"  />
								<ButtonProduct btnClass="button-wishlist" faClass="fa fa-heart-o"  />								
							</div>
						</div>
					</div>	                
	                <div className="caption">   
	        			<h4>
	        			<a href={this.props.product.url}>{this.props.product.name}</a>                    				
	        			</h4>		
						<p className="price">{price}</p>
	        
	        		</div>
                </div>
            </div>
        );
    }
}

function ButtonProduct(props) {
	  return (
			<button type="button" className={props.btnClass} >
				<i className={props.faClass}></i> {props.label}
			</button>	  
	  );
}

class ProductList extends React.Component {

	getImgScr(){
		return '/files/img/product/' +  this.props.product.img;
	}
	
	getStars(){
		return (
				<div>
					//for()
					<span className="icon-ratings"><i className="fa fa-star"></i></span>
					<span className="icon-ratings"><i className="fa fa-star"></i></span>
					<span className="icon-ratings"><i className="fa fa-star"></i></span>
					<span className="icon-ratings"><i className="fa fa-star-o"></i></span>
					<span className="icon-ratings"><i className="fa fa-star-o"></i></span>	
				</div>
		);
	}
	
    render() {
		var formatter = new Intl.NumberFormat('ru-RU', {
			  style: 'currency',
			  currency: 'RUB',
			});
		var price = formatter.format(this.props.product.price);
    	var info = this.props.product.info;
    	if(this.props.product.info){
    		if(this.props.product.info.length > 250){
    			var info = this.props.product.info.substr(0, 250) + '...';
    		}
    	}
    	
        return (
            <div className="list-style">
	            <div className="product-item">
		            <div className="image rotate-image-container ">
			            <a href={this.props.product.url}>
			                <img className="img-responsive" src={this.getImgScr()} alt="Product" />
			            </a>    
					</div>	
					<div className="caption">   
		     			<h4>
		     			<a href={this.props.product.url}>{this.props.product.name}</a>                    				
		     			</h4>		
						<p className="price">{price}</p>
						<div className="rating">{this.getStars()}</div>
						<p className="product-description">{info}</p>
						
						<div className="button-group">	                    		
							<div className="inner">
								<ButtonProduct btnClass="button-cart" faClass="fa fa-cart-plus" label="Add to Cart" /> 
								<ButtonProduct btnClass="button-quickview" faClass="fa fa-search"  />
								<ButtonProduct btnClass="button-wishlist" faClass="fa fa-heart-o"  />								
							</div>
						</div>
						
		     		</div>
	            </div>
            </div>
        );
    }
}


//Pagenation
class Pagenation extends React.Component {

    render() {
        var countAllProducts = this.props.countAllProducts;
        var countProducts = this.props.countProducts;
        var currentPage = this.props.currentPage;
        const pageNumbers = [];
        var startPage = 1;
        var endPage = Math.ceil(countAllProducts / countProducts);


        if (Math.ceil(countAllProducts / countProducts) > 1) {
            if (currentPage > 1 && currentPage < Math.ceil(countAllProducts / countProducts)) {
                startPage = currentPage - 1;
                endPage = startPage + 2;
                if (endPage > Math.ceil(countAllProducts / countProducts)) {
                    endPage = Math.ceil(countAllProducts / countProducts);
                }
                if (startPage < 1) {
                    startPage = 1;
                }

            } else if (currentPage >= Math.ceil(countAllProducts / countProducts)) {
                startPage = currentPage - 2;
                endPage = currentPage;
                if (endPage > Math.ceil(countAllProducts / countProducts)) {
                    endPage = Math.ceil(countAllProducts / countProducts);
                }
                if (startPage < 1) {
                    startPage = 1;
                }
            } else {
                startPage = 1;
                endPage = 3;
                if (endPage > Math.ceil(countAllProducts / countProducts)) {
                    endPage = Math.ceil(countAllProducts / countProducts);
                }
                if (startPage < 1) {
                    startPage = 1;
                }
            }

            for (let i = startPage; i <= endPage; i++) {
                pageNumbers.push(i);
            }

            const renderPageNumbers = pageNumbers.map(number => {
                if (currentPage == number) {
                    return (
                        <li className="page-item active" key={number} id={number} onClick={this.props.onClickNumPage}>
                            <button className="page-link" href="">{number}</button>
                        </li>
                    );
                } else {
                    return (
                        <li className="page-item" key={number} id={number} onClick={this.props.onClickNumPage}>
                            <button className="page-link" href="">{number}</button>
                        </li>
                    );
                }
            });

            return (
                <div className="pagination-block">
                    <ul className="pagination">
                        <li className="page-item prev disabled" onClick={this.props.onClickPrev}>
                            <button className="page-link"> &laquo; </button>
                        </li>

                        {renderPageNumbers}

                        <li className="page-item prev disabled" onClick={this.props.onClickNext}>
                            <button className="page-link"> &raquo; </button>
                        </li>

                    </ul>
                </div>
            );
        } else {
            currentPage = 1;
            return <div className="pagination-block"></div>;
        }


    }
}


//WidgetProductsBlock
class WidgetProductsBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            countProducts: $.cookie('pageProductCount') == null ? 12 : $.cookie('pageProductCount'),
            currentPage: 1,
            typeShowBlock: 'grid',
        };

        this.changeShow = this.changeShow.bind(this);
        this.onClickShowBlock = this.onClickShowBlock.bind(this);
    }

    changeShow(event) {
        $.cookie('pageProductCount', event.target.value);
        this.setState({
            countProducts: event.target.value,
        });
    }

    onClickShowBlock(target) {
        this.setState({
            typeShowBlock: target.dataset.showblock,
        });
    }

    render() {
        const {countProducts, currentPage, typeShowBlock} = this.state;
        return (
            <div>
                <ToolBar countProducts={countProducts} onClickShowBlock={this.onClickShowBlock}
                         onChangeShow={this.changeShow}/>
                <Products products={this.props.products} typeShowBlock={typeShowBlock} currentPage={currentPage} countProducts={countProducts}/>
            </div>
        )
    }
}


//InfoCategory
class InfoCategory extends React.Component {
	
    render() {
    	var category = this.props.category;
        return (
            <div>
            	<h2>{category.name}</h2>
            	<hr />
            	<p>{category.info}</p>
            	<br />
            </div>
        )
    }
}


class Filter extends React.Component {
	  constructor(props) {
			super(props);
			this.state = {
				priceMax: 0,
				priceMin: 0,
			};
			this.priceChangeMin = this.priceChangeMin.bind(this);
			this.priceChangeMax = this.priceChangeMax.bind(this);

	   }
	  	  
	 priceChangeMin(e){
		 this.setState({
			 priceMin: e.target.value
		 });
	 } 
	 priceChangeMax(e){
		 this.setState({
			 priceMax: e.target.value
		 });
	 } 
	 

	 getMaxPriceDef(){
         let products = this.props.products;
		 var max = 0;
         products.forEach(function (product) {        	 	
	        	 if(product.price > max)
	        		 max = product.price;
	         } 
         );
         
         return max;
    }
	 
	 getMinPriceDef(){
         let products = this.props.products;
		 var min = 10000000;
         products.forEach(function (product) {        	 	
	        	 if(product.price < min)
	        		 min = product.price;
	         } 
         );
         console.log(min);
         return min;
    }
	  
	
	 render() { 
		 var {priceMin, priceMax} = this.state;
		 const maxPriceDef = this.getMaxPriceDef();
		 const minPriceDef = this.getMinPriceDef();
		 
		 priceMax = priceMax <= 0 ? maxPriceDef : priceMax; 
		 priceMin = priceMin <= 0 ? minPriceDef : priceMin; 
		 
		 return( 
			 <div className="filter">
				 <div className="filter-attribute-container">
					<label>Цена</label>
					<div className="list-group-item">
						<span>От </span>	
						<span>
							<input
								type="text" 
								onChange={this.priceChangeMin} 
								className="form-control" 
								value={priceMin}
								min={minPriceDef}
								max={maxPriceDef}
							/>						
						</span>
						<span>До </span>
							<span>
							<input
								type="text" 
								onChange={this.priceChangeMax} 
								className="form-control" 
								value={priceMax}
								min={minPriceDef}
								max={maxPriceDef}
							/>		
						</span>
					</div>
				 </div>
			 </div>	
		 );
	 }
}

class PageRender extends React.Component {
  constructor(props) {
		super(props);
		this.state = {
			products: [],
			category: []
		};

   }
	
    ajaxQuery() {
        var _data = null;
        $.ajax({
            url: window.location.href,
            dataType: "json",
            method: "POST",
            async: false,
        }).done(function (data) {
            _data = data;
            console.log(_data);
        })
        return _data;
    }
    
    componentDidMount() {
        var data = this.ajaxQuery();
        this.setState({
        	products: data.products,
        	category: data.category,
        });
    }
	
	 render() {
		 const {products, category} = this.state;	

		 return( 
			 <div className="container">
				 <div className="row">	
				 	<aside id="column-left" className="col-sm-3 col-xs-12">
				 		<Filter 
				 			products={products}
				 		/>
				 	</aside>
				 	<div className="col-sm-9">
				 		<InfoCategory category={category} />
				 		<WidgetProductsBlock products={products} />
				 	</div>
				 </div>	
			 </div>	
		 )
	    }
}

//Render
if ($('.jsx-view-catalog')[0] != null) {
    ReactDOM.render(
        <PageRender />,
        $('.jsx-view-catalog')[0]
    );
}