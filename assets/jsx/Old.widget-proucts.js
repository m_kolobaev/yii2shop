
//this.props.faClass	
//this.props.active
class LeftButton extends React.Component {
	render (){
		var ClassActive = this.props.active == "on" ? 'active' : '';
		var classNew= "btn btn-default btn-custom-view btn-grid-1" + ' ' + ClassActive;
		return <button type="button" onclick="" className={classNew} onClick={this.props.onClick}><i className={this.props.faClass}></i></button>		
	}	
}

// ShowBlock
class ShowBlock extends React.Component {
	render(){
		var ShowLimitProducts = [4,12,26,52,74];
		var optionsArr = ShowLimitProducts.map(function(num) {
		 	  return <option key={num} value={num}>{num}</option>;
		 	});		
		return <div className="form-group input-group input-group-sm ">
					<label className="input-group-addon" htmlFor="input-limit-products">Show:</label>
					<select id="input-limit-products" className="form-control" onChange={this.props.onChange} >
						{optionsArr}
					</select>
				</div>;
	}		
}	
	
// SortByBlock
class SortByBlock extends React.Component {	
	render(){
		var SortBy = ["name", "price", "stars"];
		 var optionsArr = SortBy.map(function(SortByParam) {
		 	  return <option key={SortByParam} value={SortByParam}>{SortByParam}</option>;
		 	});
		
		return <div className="form-group input-group input-group-sm ">
			 		<label className="input-group-addon" htmlFor="input-sort">Sort By:</label>
			 		<select id="input-sort" className="form-control">
			 			{optionsArr}
			 		</select>
			   </div>;
	}		
}

class LeftButtonControl extends React.Component {
	 constructor(props) {
	    super(props);
	    this.state = {stateShow: 'grid'};
	    
	    this.gridClick = this.gridClick.bind(this);
	    this.listClick = this.listClick.bind(this);
	  }
	 
	  gridClick() {
		  this.setState({stateShow: 'grid'});
	  }
	  
	  listClick() {
		  this.setState({stateShow: 'list'});
	  }
	
	render(){
		const stateShow = this.state.stateShow;
		if( stateShow =='grid' ){
			return (
					<div className="btn-group btn-group-sm">
						<LeftButton faClass="fa fa-th" active="on" onClick={this.gridClick} />
						<LeftButton faClass="fa fa-th-list" onClick={this.listClick} />
					</div>
					);
		}else{
			return (
					<div className="btn-group btn-group-sm">
						<LeftButton faClass="fa fa-th" onClick={this.gridClick} />
						<LeftButton faClass="fa fa-th-list"  active="on" onClick={this.listClick} />
					</div>
					);
		}
		
		
	}
}
// ToolBar
class ToolBar extends React.Component {	
	/* constructor(props) {
		    super(props);
		    this.state = {stateShow: 'grid'};
		    
		    this.changeShow = this.changeShow.bind(this);
		  }
	
	changeShow(event){
		console.log(event.target.value);
	}
	*/
	render() {
		return <div className="tool-bar row">
					<div className="col-md-6 col-xs-6">						
						<LeftButtonControl />	
					</div>
					<div className="col-md-3 col-xs-3 text-right">
						<SortByBlock />
					</div>
					<div className="col-md-3 col-xs-3 text-right">
						<ShowBlock todosPerPage={todosPerPage} onChange={this.props.onChange} />
					</div>
				</div>;
	  }
}

class ProductImgButtonGroupBtn extends React.Component {
	render(){
		return <button type="button" className="button-cart" onclick="" title="Add to Cart">
			<i className={this.props.faClass}></i>
		</button>
	}
	
}
class ProductImgButtonGroup extends React.Component {
	render(){
		return <div className="button-group">
					<div className="inner">
						<ProductImgButtonGroupBtn faClass="fa fa-cart-plus" />
						<ProductImgButtonGroupBtn faClass="fa fa-search" />
						<ProductImgButtonGroupBtn faClass="fa fa-heart-o" />
					</div>
				</div>
					
		
	}
}
class ProductImg extends React.Component {
	render(){
		return <div className="image ">
					<a href=""> 
						<img src={this.props.scr} alt="Product" />
					</a>
					<ProductImgButtonGroup />
				</div>
				
					
	}
}

class Caption extends React.Component {
	render(){
		
		var formatter = new Intl.NumberFormat('ru-RU', {
			  style: 'currency',
			  currency: 'RUB',
			});
		
		var $price = formatter.format(this.props.price);
		
		return <div className="caption">        
					<h4>
						<a href={this.props.url}>SAMSUNG Galaxy A40 64Gb</a>                    				
					</h4>
					<p className="manufacture-product">
						<a href="http://safira.demo.towerthemes.com/htc">HTC</a>
					</p>							
						
						<p className="price">{$price}</p>
			
				</div>
		
		
	}
}

class ProductItem extends React.Component {
	render(){
		var ImgScr = '/files/img/product/'+ this.props.product.img; 
		return   <div  className="product-item">						
						<ProductImg scr = {ImgScr} />						
						<Caption url = {this.props.product.url} price={this.props.product.price} />
		        	</div>
	}
}



class Products extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	      error: null,
	      isLoaded: false,
	      products: [],
	      
	    };
	  }
	
	ajaxQuery(){
		 var _this = this;
		 var _data = null; 
		 $.ajax({
			  url: "http://0newyii2:8080/catalog/view-test?id=13",
			  dataType: "json",
			  method: "POST",
			  async: false,
			}).done(function(data) {
				_data = data;
				
			})
			return _data;
	}
	
	 componentDidMount() {
		var data = this.ajaxQuery();
		
		this.setState({
			            isLoaded: true,
			            products: data.products
			          }); 
		
	 }

	
	render(){
		const { error, isLoaded, products } = this.state;
		if (error) {
	      return <div>Ошибка: {error.message}</div>;
	    } else if (!isLoaded) {
	      return <div>Загрузка...</div>;
	    } else {
	    	const currentTodos = products.slice(0, this.props.countProducts);
	    	
	        return (		        		
	        		<div>
		        		<Pagenation countElements={products.length} />	  
		                <div className="row product-thumb">	                              
		                {
		                	currentTodos.map((product, index) => (
	            			    <div key={product.id} className="col-sm-3">	  	
	            					<ProductItem  product = {product} />
	                			</div>
		                	  ))
		                  }
		                </div>
	                </div>
	              );
        }
		
		return <h1>{this.props.text}  </h1>
	}
	
}

class Pagenation extends React.Component {
	constructor(props) {
		 super(props);
	     this.state = {
	       countElements: props.countElements,
	       currentPage: 1,
	       todosPerPage: 12
	     };
     }
	

	render(){
	 const { countElements, currentPage, todosPerPage } = this.state;
		
	 const pageNumbers = [];
        for (let i = 1; i <= Math.ceil( countElements / todosPerPage); i++) {
          pageNumbers.push(i);
        }
        
        const renderPageNumbers = pageNumbers.map(number => {
            return (
              <span
                key={number}
                id={number}
                onClick={this.props.onClick}
              >
                {number}
              </span>
            );
          });
		
		return (
				<div>
					<hr />
			        {renderPageNumbers}
			        <hr />
	        	</div>
        );
	}
}


	class WidgetProductsBlock extends React.Component {	
		constructor(props) {
			 super(props);
		     this.state = {
		       countProducts: 12,
		     };
		     
		     this.changeShow = this.changeShow.bind(this);
	     }
		
		changeShow(event){
			this.setState({
				countProducts: event.target.value,
	          }); 
		}
		
		render(){
			return <div>					
					  <ToolBar todosPerPage=12 onChange={this.changeShow} />
					  <Products todosPerPage=12 countProducts = {this.state.countProducts} />
				   </div>
			}
		}
	

	
// render

if($('.jsx-widget-proucts')[0] != null){
	ReactDOM.render(	
			<WidgetProductsBlock />,
			$('.jsx-widget-proucts')[0]		
		);	
}
