<?php
namespace app\widgets;

use yii\widgets;
use app\models\Categories;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Виджет для вывода дерева разделов каталога товаров
 */
class TreeWidget extends Widget
{

    /**
     * Выборка категорий каталога из базы данных
     */
    protected $categories;

    /**
     * Массив категорий каталога в виде дерева
     */
    protected $tree;

    public function run()
    {
        $this->categories = Categories::find()->indexBy('id')
            ->asArray()
            ->all();
        
        $this->makeTree();
        
       /*
        $dataCategories = $cache->getOrSet($key, function () use ($user_id) {
            return $this->calculateSomething($user_id);
        });
        */

        if (! empty($this->tree)) {
            $html = $this->HtmlTree($this->tree);
        } else {
            $html = '';
        }

        return $html;
    }

    /**
     * Функция принимает на вход линейный массив элеменов, связанных
     * отношениями parent-child, и возвращает массив в виде дерева
     */
    protected function makeTree()
    {
        if (empty($this->categories)) {
            return;
        }
        foreach ($this->categories as $id => &$node) {
            if (! $node['parent_id']) {
                $this->tree[$id] = &$node;
            } else {
                $this->categories[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
        unset($node);
    }

    protected function HtmlTree($tree)
    {
        $html = '';

        $html .= Html::beginTag('ul');
        foreach ($tree as $itemTree) {
            $htmlBadge = '';
            $htmlChilds = '';
            if (isset($itemTree['childs'])) {
                $htmlBadge = Html::tag('i', '', [
                    'class' => 'fa fa-angle-right'
                ]);
                $htmlChilds = $this->HtmlTree($itemTree['childs']);
            }
            $html .= Html::tag('li', Html::a($itemTree['name'], [
                'catalog/view',
                'id' => $itemTree['id']
            ]) . $htmlBadge . $htmlChilds, [
                'class' => 'category-item'
            ]);
        }
        $html .= Html::endTag('ul');
        return $html;
    }
}