<?php
namespace app\controllers;

use app\models\Products;
use yii\web\NotFoundHttpException;

class ProductController extends \yii\web\Controller
{

    public function actionView($id)
    {
        $product = $this->findModel($id);

        $productOptionVar = $product->optionsVar;
        $productOptionVarArr = array();
        $reviews = $product->reviews;
        $countReviews = count($reviews);

        foreach ($reviews as $reviewsStars) {
            $reviewsRes += $reviewsStars->stars;
        }
        $reviewsRes = ($countReviews > 0) ? $reviewsRes / $countReviews : 0;

        foreach ($productOptionVar as $p) {
            $productOptionVarArr[$p->option->name][] = $p->name;
        }

        return $this->render('view', [
            'product' => $product,
            'productOptionVarArr' => $productOptionVarArr,
            'reviews' => $reviews,
            'reviewsRes' => $reviewsRes,
            'countReviews' => $countReviews
        ]);
    }

    protected function findModel($id)
    {
        $model = Products::find()->where('id=:id', [
            ':id' => $id
        ])
            ->with([
            'category'
        ])
            ->one();
        if (! empty($model)) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private $breadcrumbsArrRes = [];
    public function getProductBreadcrumbs($category, $arr = [])
    {        
        $cache = \Yii::$app->cache;
        $arr = [
            'label' => $category->name, 
            'url' => $this->parentCategory($category, $category->slug)        
        ];
       
        
        if (isset($category->parent_id)) {
            
            $cid = $category->parent_id;
            $key = 'CategoryID-' . $cid;
            $categoryParent = $cache->getOrSet($key, function () use ($category) {
                return $category->parent;
            }, 3600);
            
                $this->getProductBreadcrumbs($categoryParent, $arr);  
        }
        $this->breadcrumbsArrRes[] = $arr;
        $this->breadcrumbsArrRes[] = '/';
        
        return $this->breadcrumbsArrRes;
    }

    private function parentCategory($category, $slug = '')
    {
        $cache = \Yii::$app->cache;
        if (isset($category->parent_id)) {
            $cid = $category->parent_id;
            $key = 'CategoryID-' . $cid;
            $categoryParent = $cache->getOrSet($key, function () use ($category) {
                return $category->parent;
            }, 3600);

            $slug = $categoryParent->slug . '/' . $slug;
            $slug = $this->parentCategory($categoryParent, $slug);
        }
        return $slug;
    }
}
