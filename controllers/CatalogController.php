<?php

namespace app\controllers;

use Yii;
use app\models\Categories;
use app\models\Brands;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use app\models\Products;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class CatalogController extends \yii\web\Controller
{
    /**
     * Главная страница каталога товаров
     */
    public function actionIndex() {
        // получаем корневые категории
        return $this->render('index');
    }
    
    public function actionViewTest($id){
        
        $category = $this->findModel($id);   
        $productsArr = [];
        
        $request = Yii::$app->request;
        if ( $request->isPost) {  
            $products = $category->products;   
            $reviewsStarsProducts = $this->countStars($products);
            
         //   print_r($reviewsStarsProducts);
          //  die;
            
            $productsArr['category'] = ArrayHelper::toArray($category);
            $productsArr['products'] = ArrayHelper::toArray($products);
            foreach($productsArr['products'] as &$product){
                $product['url'] = Url::to(['product/view', 'id' => $product['id']]);
                $product['stars'] = $reviewsStarsProducts[$product['id']];
            }
            unset($product);
            return json_encode ($productsArr);
        }
        
        
        return $this->render('view-test', ['category' => $category,]);
    }
    
    private function countStars ($products){
        $reviews = null;
        $reviewsStarsProducts = [];
        $countReviews = 0;
        $reviewsRes = 0;
        
        foreach ($products as $product) {
            $reviews = $product->reviews;
            $countReviews = count($reviews);
            foreach ($reviews as $reviewsStars) {
                $reviewsRes += $reviewsStars->stars;
            }
            $reviewsRes = ($countReviews > 0) ? $reviewsRes / $countReviews : 0;
            
            $reviewsStarsProducts[$product->id] = $reviewsRes;
        }
       
        return $reviewsStarsProducts;
    }
    
    public function actionView($id)
    {
        $cookies = \Yii::$app->request->cookies;
        $pageProductCount = 10;
        if ($cookies->get('pageProductCount') !== null) {
            $pageProductCount = $cookies->get('pageProductCount')->value;
        }
        
        
        $category = $this->findModel($id);     
        $pages = new Pagination(['totalCount' => $category->getCountProducts(), 'pageSize' => $pageProductCount]);
        
        $category->ofsetProducts = $pages->offset;
        $category->limitProducts = $pages->limit;
        
        
        $products = $category->products;
        
        
        return $this->render('view', [
            'category' => $category,
            'products' => $products,
            'pages' => $pages
        ]);
    }
    
    
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    
    private $breadcrumbsArrRes = [];
    public function getCatalogBreadcrumbs($category, $arr = [])
    {
        $cache = \Yii::$app->cache;
        $arr = [
            'label' => $category->name,
            'url' => $this->parentCategory($category, $category->slug)
        ];
        
        
        if (isset($category->parent_id)) {
            
            $cid = $category->parent_id;
            $key = 'CategoryID-' . $cid;
            $categoryParent = $cache->getOrSet($key, function () use ($category) {
                return $category->parent;
            }, 3600);
                
                $this->getCatalogBreadcrumbs($categoryParent, $arr);
        }
        $this->breadcrumbsArrRes[] = $arr;
        $this->breadcrumbsArrRes[] = '/';
        
        return $this->breadcrumbsArrRes;
    }
    
    private function parentCategory($category, $slug = '')
    {
        $cache = \Yii::$app->cache;
        if (isset($category->parent_id)) {
            $cid = $category->parent_id;
            $key = 'CategoryID-' . $cid;
            $categoryParent = $cache->getOrSet($key, function () use ($category) {
                return $category->parent;
            }, 3600);
                
                $slug = $categoryParent->slug . '/' . $slug;
                $slug = $this->parentCategory($categoryParent, $slug);
        }
        return $slug;
    }
    
    
    

}
