<?php
use app\widgets\TreeWidget;
?>
<nav id="top">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">

				<div class="btn-group">
					<button type="button" class="btn dropdown-toggle"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RUS</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">ENG</a> <a class="dropdown-item"
							href="#">GER</a> <a class="dropdown-item" href="#">RUS</a>

					</div>
				</div>

				<div class="btn-group">
					<button type="button" class="btn dropdown-toggle"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">€
						Euro</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">€ Euro</a> <a
							class="dropdown-item" href="#">£ Pound Sterling</a> <a
							class="dropdown-item" href="#">$ US Dollar</a>

					</div>
				</div>




			</div>
			<div class="col-sm-9 text-right">
				<div class="social">
					<a href=""><i class="fa fa-facebook"></i></a> <a href=""><i
						class="fa fa-whatsapp"></i></a> <a href=""><i class="fa fa-vk"></i></a>
					<a href=""><i class="fa fa-instagram"></i></a>
				</div>
			</div>
		</div>
	</div>
</nav>
<header>
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<div id="logo">
					<a href="/"> <img
						src="http://safira.demo.towerthemes.com/image/catalog/logo/logo.png"
						title="" alt="" class="img-responsive"></a>
				</div>
			</div>
			<div class="col-sm-4">
    			<nav class="search-form">
    				<form class="form-inline search">
    					<input class="form-control mr-sm-2" type="search"
    						placeholder="Search" aria-label="Search">
    					<a><i class="fa fa-search" aria-hidden="true"></i></a>
    				</form>
    			</nav>
			</div>
			<div class="col-sm-2 text-right">
				<div class="register-or-login">
					<a>REGISTER </a> /	<a>LOGIN </a>
				</div>				
			</div>
			<div class="col-sm-2 text-right">
				<div class="right-bar">
					<a href=""><i class='fa fa fa-heart-o'></i> <span class="txt-count">0</span></a> 
					<a href=""><i class="fa fa-shopping-cart"></i> <span class="txt-count">0</span></a>				
				</div>
			</div>
		</div>
	</div>

	<div class="main-menu">
		<div class="container">
    		<div class="row">
        		<div class="col-sm-3">
    				<div class="btn-group">
    					<button type="button" class="btn dropdown-toggle menu-bar-categoies" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    						<i class="fa fa-bars"></i>All Cattegories
    					</button>
    					
    					<div class="dropdown-menu cattegories-items">
    							<?= TreeWidget::widget();  ?>
    					</div>
    				</div>
        		</div>
        		<div class="col-sm-6">

					<ul class="ul-top-items">
						<li class="li-top-item left  active" style="float: left">
							<a class="a-top-link" href="/"> <span>Главная</span></a>
						</li>
						<li class="li-top-item left  " style="float: left">
							<a class="a-top-link" href="/"> <span>Акции</span></a>
						</li>
						<li class="li-top-item left  " style="float: left">
							<a class="a-top-link" href="/"> <span>Контакты</span></a>
						</li>
						<li class="li-top-item left  " style="float: left">
							<a class="a-top-link" href="/"> <span>О нас</span></a>
						</li>
						
					</ul>
				</div>
				
				<div class="col-sm-3 text-right">
    				<div class="header-phone">
    					<a href="">
    					<i class="fa fa-phone-square"></i>
    						<span><span>(08) 23 456 789</span>Customer Support</span>
    					</a>
    				</div>
				</div>
    		</div>
		</div>
	</div>



</header>





