<?php
use yii\bootstrap4\LinkPager;
use yii\helpers\Html;

$this->title = $category->name;
$this->params['breadcrumbs'] =  $this->context->getCatalogBreadcrumbs($category->parent);
$this->params['breadcrumbs'][] = $this->title;

?>







<div class="row">
	<aside id="column-left" class="col-sm-3 col-xs-12">
		<div class="col-order-inner">
			<div class="panel panel-default pt-filter">
				<div class="layered">
					<div class="list-group">
						<div
							class="filter-attribute-container filter-attribute-remove-container">
						</div>
						<div class="filter-attribute-container">
							<label>Цена</label>
							<div class="list-group-item">
								<span>От </span>	<span><input type="range" class="custom-range" value="450" min="0" max="10000"></span>
								
								<span>До </span>	<span><input type="range" class="custom-range" value="9000" min="0" max="10000"></span>
							</div>
						</div>
						<div class="filter-attribute-container">
							<label>Color</label>
							<div class="list-group-item">
								<div id="filter-group2">
									<a class="a-filter add-filter" href="javascript:void(0);"
										name="7">Black (11)</a> <a class="a-filter add-filter"
										href="javascript:void(0);" name="13">Blue (13)</a> <a
										class="a-filter add-filter" href="javascript:void(0);"
										name="9">Gray (7)</a> <a class="a-filter add-filter"
										href="javascript:void(0);" name="10">Green (10)</a> <a
										class="a-filter add-filter" href="javascript:void(0);"
										name="14">Orange (12)</a> <a class="a-filter add-filter"
										href="javascript:void(0);" name="11">Red (12)</a> <a
										class="a-filter add-filter" href="javascript:void(0);"
										name="12">Violet (12)</a> <a class="a-filter add-filter"
										href="javascript:void(0);" name="8">White (10)</a>
								</div>
							</div>
						</div>
						<div class="filter-attribute-container">
							<label>Manufacturers</label>
							<div class="list-group-item">
								<div id="filter-group1">
									<a class="a-filter add-filter" href="javascript:void(0);"
										name="1">Apple (13)</a> <a class="a-filter add-filter"
										href="javascript:void(0);" name="2">Canon (9)</a> <a
										class="a-filter add-filter" href="javascript:void(0);"
										name="3">Hewlett-Packard (11)</a> <a
										class="a-filter add-filter" href="javascript:void(0);"
										name="4">HTC (10)</a> <a class="a-filter add-filter"
										href="javascript:void(0);" name="5">Palm (6)</a> <a
										class="a-filter add-filter" href="javascript:void(0);"
										name="6">Sony (11)</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>




			<div class="banner-left">
				<a href="#"><img src=""
					alt="banner-left"></a>
			</div>


		</div>
	</aside>


	 <div class="col-sm-9">
	 <h2><?= $category->name; ?></h2>
		<hr>
		<div class="row">
			<div class="col-sm-10">
				<p> <?= $category->info; ?> </p>
			</div>
		</div>

		<br>





		<div class="tool-bar">
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<div class="btn-group btn-group-sm">
						<button type="button"
							onclick=""
							class="btn btn-default btn-custom-view btn-grid-1 active"><i class="fa fa-th"></i></button>
						<button type="button"
							onclick=""
							class="btn btn-default btn-custom-view btn-grid-1"><i class="fa fa-th-list"></i></button>
						
						<input type="hidden" id="category-view-type" value="grid"> <input
							type="hidden" id="category-grid-cols" value="3">
					</div>
				</div>
				<div class="col-md-3 col-xs-6 text-right">
					<div class="form-group input-group input-group-sm">
						<label class="input-group-addon" for="input-sort">Sort By:</label>
						<select id="input-sort" class="form-control">
							<option
								selected="selected">Default</option>
							<option
								value="">Name</option>
							
							<option>Price</option>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-xs-6 text-right">
					<div class="form-group input-group input-group-sm ">
						<label class="input-group-addon" for="input-limit-products">Show:</label>
						<select id="input-limit-products" class="form-control">
							<option value="4" >4</option>
							<option value="12">12</option>
							<option value="26">26</option>
							<option value="52">52</option>
							<option value="74">74</option>
						</select>
					</div>
				</div>
			</div>
		</div>

<div class="pagination-block">
    <?= LinkPager::widget([
     'pagination' => $pages,
    ]); ?>
</div>

    	 <div class="row product-thumb">
    <?php
    ?>
         <?php foreach ($products as $product):?>
                <div class="col-sm-3">
                    <div class="grid-style">
                    			<div class="product-item">
                    				<div class="image ">
                    					<a href=""> <img class="img-responsive" src="/files/img/product/<?= $product->img ?>" alt="Product">
                    					</a>
                    					<div class="button-group">
                    						<div class="inner">
                    							<button type="button" class="button-cart"
                    								onclick="" title="Add to Cart">
                    						<i class="fa fa-cart-plus"></i>
                    					</button>
                    					<button type="button" title="Quick View"
                    						class="button-quickview"
                    						onclick="">
                    						<span><i class="fa fa-search"></i></span>
                    					</button>
                    					<button type="button" title="Add to Wish List"
                    						class="button-wishlist" onclick="">
                    						<span><i class="fa fa-heart-o"></i></span>
                    					</button>
                    				</div>
                    			</div>
                    		</div>
                    		<div class="caption">
                    
                    
                    
                    			<h4>
                    			<?= Html::a($product->name,[
                    			    'product/view',
                    			    'id' => $product->id
                    			]); ?>
                    				
                    			</h4>
                    			<p class="manufacture-product">
                    				<a href="http://safira.demo.towerthemes.com/htc">HTC</a>
                    			</p>							
                    			
        						<p class="price"><?= number_format($product->price, 2, ',', ' '); ?> P</p>
                    
                    		</div>
                    	</div>
                    </div>
                
                </div>
                <?php endforeach;   ?>
        </div>
<div class="pagination-block">
    <?= LinkPager::widget([
     'pagination' => $pages,
    ]); ?>
</div>
    </div>

</div>