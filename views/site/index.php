<?php

use app\models\Products;
use app\widgets\TreeWidget;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>



<section class="">
	<div class="banner-static">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-sms-12">
				<div class="col col1">
					<div class="banner-icon">
						<i class="fa fa-home"></i>
					</div>
					<div class="banner-content">
						<h2>Бесплатная доставка</h2>
						<p>Бесплатная доставка при заказе на сумму свыше 5000 рублей.</p>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-sms-12">
				<div class="col col2">
					<div class="banner-icon">
						<i class="fa fa-clock-o"></i>
					</div>
					<div class="banner-content">
						<h2>Поддержка 24/7</h2>
						<p>Наши операторы готовы помочь вам круглосуточно 7 дней в неделю</p>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-sms-12">
				<div class="col col3">
					<div class="banner-icon">
						<i class="	fa fa-refresh"></i>
					</div>
					<div class="banner-content">
						<h2>30 Дней возврата</h2>
						<p>Мы предоставляем возврат товара или обмен в течение 30 дней</p>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-sms-12">
				<div class="col col4">
					<div class="banner-icon">
						<i class="	fa fa-credit-card"></i>
					</div>
					<div class="banner-content">
						<h2>100% Безопасная оплата</h2>
						<p>Вы можите оплатить товар после получения и проверки заказа</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>



<section class="products-container  tabs-product">

	<div class="block-title">
		<p>Новинки на нашем сайте</p>
		<h3>
			<span>Популярные продукты</span>
		</h3>
	<div class="tabs-style">
				<ul class="nav nav-tabs">
																<li class="active">
							<a href="" data-tab="#tab-product-167-0">
								                                <span>Vegetables</span>
							</a>
						</li>
																	<li>
							<a href="" data-tab="#tab-product-167-1">
								                                <span>Fruits</span>
							</a>
						</li>
																	<li>
							<a href="" data-tab="#tab-product-167-2">
								                                <span>Salads</span>
							</a>
						</li>
															</ul>
			</div>
	
	 <div class="row product-thumb">
	    <?php
           $topProiducts = Products::find()->limit(6)->orderBy('id DESC')->all();
        ?>
	 <?php foreach ($topProiducts as $topProduct):?>
	 
        <div class="col-sm-2">
            <div class="grid-style">
            			<div class="product-item">
            				<div class="image ">
            					<a href=""> <img src="/files/img/product/<?= $topProduct->img ?>" alt="Product">
            					</a>
            					<div class="button-group">
            						<div class="inner">
            							<button type="button" class="button-cart"
            								onclick="" title="Add to Cart">
            						<i class="fa fa-cart-plus"></i>
            					</button>
            					<button type="button" title="Quick View"
            						class="button-quickview"
            						onclick="">
            						<span><i class="fa fa-search"></i></span>
            					</button>
            					<button type="button" title="Add to Wish List"
            						class="button-wishlist" onclick="">
            						<span><i class="fa fa-heart-o"></i></span>
            					</button>
            				</div>
            			</div>
            		</div>
						<div class="caption">
							<h4>
								<a href="/product/view?id=<?= $topProduct->id; ?>"><?= $topProduct->name; ?></a>
							</h4>
							<p class="manufacture-product">
								<a href="http://safira.demo.towerthemes.com/htc">HTC</a>
							</p>
							<p class="price"><?= number_format($topProduct->price, 2, ',', ' '); ?> P</p>

						</div>
					</div>
            </div>
        
        </div>
        <?php endforeach;   ?>
    </div>
	
	
	
	
	</div>

</section>







<br>
<br>
<br>
<br>

<section class="news">
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-left">
            <h2 class="heading_space uppercase">Новинки на нашем сайте
            </h2>
            <p class="bottom_half">Новые товары для вас
            </p>
          </div>
        </div>
    
        <div class="row">
        
        <?php
        /*    $topProiducts = Products::find()->limit(3)->all();
        ?>

            <?php

        foreach ($topProiducts as $topProduct):?>
         <div class="col-sm-4">
            <a class="grid_box clearfix" href="grid_list.html">
              <div class="image_left">
                <img src="/files/img/product/<?= $topProduct->img ?>" alt="Product">
              </div>
              <div class="grid_body">
                <h2 class="uppercase"><?= $topProduct->name ?>
                </h2>
                <h4>Creative home deco ideal
                </h4>
                <h3><?= number_format($topProduct->price , 2, ',', ' ')?> P
                </h3>
              </div>
            </a>
          </div>
        <?php endforeach;  */ ?>
    
        </div>
    </div>
</section>


<section>
<!-- Start Single Product -->
    <div class="row">

<?php foreach ($topProiducts as $topProduct):?>
        <div class="col-md-3 single__pro col-lg-3 col-sm-4 col-xs-12 cat--4">
           <div class="product foo">
               <div class="product__inner">
                   <div class="pro__thumb">
                       <a href="#">
                           <img class="img-responsive" src="/files/img/product/<?= $topProduct->img ?>" alt="product images">
                       </a>
                   </div>
                   
               </div>
               <div class="product__details text-center">
                   <h2><a href="product-details.html">Brone Lamp Glasses</a></h2>
                   <ul class="product__price">
                       <li class="new__price">$10.00</li>
                   </ul>
               </div>
           </div>
        </div>
        <?php endforeach;   ?>
    </div>
<!-- End Single Product -->
</section>



<div class="site-index">

   <div class="jumbotron">
       <h1>Congratulations!</h1>

       <p class="lead">You have successfully created your Yii-powered application.</p>

       <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
   </div>

   <div class="body-content">

       <div class="row">
           <div class="col-lg-4">
               <h2>Heading</h2>

               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                   dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                   ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                   fugiat nulla pariatur.</p>

               <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
           </div>
           <div class="col-lg-4">
               <h2>Heading</h2>

               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                   dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                   ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                   fugiat nulla pariatur.</p>

               <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
           </div>
           <div class="col-lg-4">
               <h2>Heading</h2>

               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                   dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                   ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                   fugiat nulla pariatur.</p>

               <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
           </div>
       </div>

   </div>
</div>
