<?php
use yii\bootstrap4\LinkPager;
use yii\helpers\Html;
use app\models\Products;

$this->title = $product->name;
$this->params['breadcrumbs'] =  $this->context->getProductBreadcrumbs($product->category);
$this->params['breadcrumbs'][] = $this->title;

?>
<hr>


<div class="row">
	<div class="col-sm-5">
		<div class="product-img-block">
			<img class="img-responsive"
				src="/files/img/product/<?= $product->img ?>" alt="Product">
		</div>
	</div>
	<div class="col-sm-7">



		<div class="card panel-default product-info-details">
			<div class="card-body ">
				<h1><?= $product->name; ?></h1>

				<div class="rating">
					<div>
						<?= Products::getStarsIcon($reviewsRes); ?>
					</div>
					<div>
						<a href="#reviews">Посмотреть отзывы (1)</a>
					</div>
				</div>


				<p class="price"><?= number_format($product->price, 2, ',', ' '); ?> P</p>


				<hr>
				
					<?php
                        $opts = array();
                        foreach ($productOptionVarArr as $optionVarName => $optionVar) {
                            $optionsVarName = null;
                            foreach ($optionVar as $key => $optionVarName) {
                                $optionsVarName .= ($key < 1) ? $optionVarName : ', ' . $optionVarName;
                            }
                            $opts[] = Html::tag('strong', $optionVarName) . ': ' . $optionsVarName;
                        }
                    
                        echo Html::ul($opts, [
                            'class' => 'list-unstyled',
                            'encode' => false
                        ]);
                    ?>

				<hr>


				<div id="product">
					<div class="form-group">
						<!-- <label class="control-label" for="input-quantity">Qty</label> -->
						<div class="quantity-content">
							<input type="text" name="quantity" value="1" size="2"
								id="input-quantity" class="form-control">
						</div>
						<input type="hidden" name="product_id" value="36">
						<button type="button" id="button-cart"
							data-loading-text="Loading..."
							class="btn btn-primary btn-lg btn-block">Add to Cart</button>

						<div class="btn-group">
							<button type="button" class="btn btn-default btn-wishlist"
								title="Add to Wish List" onclick="wishlist.add('36');">
								<i class="fa fa fa-heart-o"></i>
							</button>

						</div>

					</div>
				</div>

				<hr>

			</div>
		</div>

		<br>
	</div>
</div>


<div id="accordion">
	<div class="btn-product-bottoms">
		<div class=btn-product-bottom>
			<div class="product-info-ac" id="product-info-ac">
				<button class="btn btn-link" data-toggle="collapse"
					data-target="#product-info-text" aria-expanded="true"
					aria-controls="product-info-text">Описание</button>
			</div>
		</div>

		<div class=btn-product-bottom>
			<div class="comment-info-text" id="comment-info-text-ac">
				<button class="btn btn-link" data-toggle="collapse"
					data-target="#comment-info-text" aria-expanded="true"
					aria-controls="comment-info-text">Отзывы (<?= $countReviews ?>)</button>
			</div>
		</div>

	</div>


	<div id="product-info-text" class="collapse show"
		aria-labelledby="product-info-text" data-parent="#accordion">
		<div class="card-body"><?= $product->info; ?></div>
	</div>
	<div id="comment-info-text" class="collapse"
		aria-labelledby="comment-info-text" data-parent="#accordion">
		<div class="card-body">
			<?php 
			    foreach($reviews as $review){
			        echo  Html::tag('div', 
			            Html::tag('div', 'имя + дата',['class' => 'card-header']) . 
			            Html::tag('div', $review->text . '<br>' .  $review->getStarsIcon() ,['class' => 'card-body']),
		            ['class' => 'card']);
			        
			        echo Html::tag('br');
			        
			    }
			?>
		
		</div>
	</div>
	<div id="collapseThree" class="collapse" aria-labelledby="headingThree"
		data-parent="#accordion">
		<div class="card-body">Что-то еще</div>
	</div>
</div>