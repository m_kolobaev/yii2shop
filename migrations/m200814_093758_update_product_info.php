<?php

use yii\db\Migration;

/**
 * Class m200814_093758_update_product_info
 */
class m200814_093758_update_product_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%products}}', 'info', $this->text());
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_111757_update_table_product cannot be reverted.\n";
        $this->dropColumn('{{%products}}', 'info');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200814_093758_update_product_info cannot be reverted.\n";

        return false;
    }
    */
}
