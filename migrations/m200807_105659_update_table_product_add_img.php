<?php

use yii\db\Migration;

/**
 * Class m200807_105659_update_table_product_add_img
 */
class m200807_105659_update_table_product_add_img extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%products}}', 'img', $this->string(255));
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_111757_update_table_product cannot be reverted.\n";
        $this->dropColumn('{{%products}}', 'img');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200807_105659_update_table_product_add_img cannot be reverted.\n";

        return false;
    }
    */
}
