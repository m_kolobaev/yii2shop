<?php

use yii\db\Migration;

/**
 * Class m200824_065031_create_product_options_var
 */
class m200824_065031_create_product_options_var extends Migration
{
    public function safeUp()
    {
      
         $this->createTable('{{%product_options_var}}', [
        // 'id' => $this->primaryKey(),
         'product_id' => $this->integer(20),
         'option_var_id' => $this->integer(20),
         ]);
         
         $this->createIndex(
         'product_id-option_var_id',
         '{{%product_options_var}}',
         ['product_id', 'option_var_id'],
         true
         );
    }
    
    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%product_options_var}}');
        
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200824_065031_create_product_options_var cannot be reverted.\n";

        return false;
    }
    */
}
