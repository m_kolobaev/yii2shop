<?php

use yii\db\Migration;

/**
 * Class m200813_072044_update_table_categories_info
 */
class m200813_072044_update_table_categories_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%categories}}', 'info', $this->text());
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_111757_update_table_product cannot be reverted.\n";
        $this->dropColumn('{{%categories}}', 'info');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200813_072044_update_table_categories_info cannot be reverted.\n";

        return false;
    }
    */
}
