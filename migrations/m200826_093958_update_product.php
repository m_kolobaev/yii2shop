<?php

use yii\db\Migration;

/**
 * Class m200826_093958_update_product
 */
class m200826_093958_update_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%products}}', 'slug', $this->string(250));
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_111757_update_table_product cannot be reverted.\n";
        $this->dropColumn('{{%products}}', 'slug');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200826_093958_update_product cannot be reverted.\n";

        return false;
    }
    */
}
