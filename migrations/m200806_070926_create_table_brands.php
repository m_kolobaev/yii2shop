<?php

use yii\db\Migration;

/**
 * Class m200806_070926_create_table_brands
 */
class m200806_070926_create_table_brands extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brands}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Наименование бренда'),
            'description' => $this->text()->comment('Описание категории'),
            'img' => $this->string(255)->comment('Имя файла изображения'),            
            'keywords' => $this->string(255)->comment('Мета-тег keywords'),
            'description' => $this->string(255)->comment('Мета-тег description'),
            
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_111757_update_table_product cannot be reverted.\n";
        $this->dropColumn('{{%brands}}', 'id_category');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200806_070926_create_table_brands cannot be reverted.\n";

        return false;
    }
    */
}
