<?php

use yii\db\Migration;

/**
 * Class m200806_101803_create_table_product_Info
 */
class m200806_101803_create_table_product_Info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products_info}}', [
            'id' => $this->primaryKey(),
            'product_id_' => $this->integer(20)->comment('Родительская категория'),
            'imgs_id' => $this->integer(20)->comment('Родительская категория'),            
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%products_info}}');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200806_101803_create_table_product_Info cannot be reverted.\n";

        return false;
    }
    */
}
