<?php

use yii\db\Migration;

/**
 * Class m200806_071202_update_table_product
 */
class m200806_071202_update_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%products}}', 'brand_id', $this->integer(20));
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_111757_update_table_product cannot be reverted.\n";
        $this->dropColumn('{{%products}}', 'id_brand');
        
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200806_071202_update_table_product cannot be reverted.\n";

        return false;
    }
    */
}
