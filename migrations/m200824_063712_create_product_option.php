<?php

use yii\db\Migration;

/**
 * Class m200824_063712_create_product_option
 */
class m200824_063712_create_product_option extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%options}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(20),
            'name' => $this->string(255)->notNull(),            
        ]);
        
        $this->createTable('{{%options_var}}', [
            'id' => $this->primaryKey(),
            'option_id' => $this->integer(20),
            'name' => $this->string(255)->notNull(),            
        ]);
       /* 
        $this->createTable('{{%product_options_var}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(20),
            'option_var_id' => $this->integer(20),
            'name' => $this->string(255)->notNull(),
        ]);
        
        $this->createIndex(
            'product_id-option_var_id',
            'post',
            'author_id',
            true
            );
            */
    }
    
    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%options}}');
        $this->dropTable('{{%options_var}}');
        
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200824_063712_create_product_option cannot be reverted.\n";

        return false;
    }
    */
}
