<?php

use yii\db\Migration;

/**
 * Class m200826_081025_update_ctegory
 */
class m200826_081025_update_ctegory extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%categories}}', 'slug', $this->string(255));
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_111757_update_table_product cannot be reverted.\n";
        $this->dropColumn('{{%categories}}', 'slug');
        
    }
    

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200826_081025_update_ctegory cannot be reverted.\n";

        return false;
    }
    */
}
