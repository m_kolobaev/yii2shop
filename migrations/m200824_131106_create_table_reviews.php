<?php

use yii\db\Migration;

/**
 * Class m200824_131106_create_table_reviews
 */
class m200824_131106_create_table_reviews extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_reviews}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(20)->notNull(),
            'user_id' => $this->integer(20)->notNull(),
            'stars' => $this->integer(10),
            'data' => $this->integer(20)->notNull(),
            'text' => $this->text()->notNull(),
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%product_reviews}}');
        
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200824_131106_create_table_reviews cannot be reverted.\n";

        return false;
    }
    */
}
