<?php

use yii\db\Migration;

/**
 * Class m200805_110605_create_table_categories
 */
class m200805_110605_create_table_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(20)->comment('Родительская категория'),
            'name' => $this->string(255)->notNull()->comment('Наименование категории'),
            'description' => $this->text()->comment('Описание категории'),            
            'img' => $this->string(255)->comment('Имя файла изображения'),
            
            'keywords' => $this->string(255)->comment('Мета-тег keywords'),
            'description' => $this->string(255)->comment('Мета-тег description'),           
            
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%categories}}');
        
    }

}
